from django.db import models
from django.db.models.fields import BooleanField
from django.utils import timezone

# Create your models here.
class ToDo(models.Model):
    event_name = models.CharField(max_length=100)
    description = models.TextField(max_length=300)
    start_date = models.DateTimeField('event start')
    create_date = models.DateTimeField('create date', auto_now=True)
    participate = models.BooleanField('participated?', default=False)
    def __str__(self):
        return self.event_name