from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_check_boolean(value):
    if value and value != 'on':
        raise ValidationError(
            _('%(value)s is not value we expected.'),
            params={'value': value},
        )