from todolist.validateAlgorithm import validate_check_boolean
from django.forms import widgets
from .models import ToDo
from django import forms

class ToDoForm(forms.ModelForm):
    class Meta:
        model = ToDo
        fields = ('event_name','description','start_date')
        widgets = {
            'event_name': forms.TextInput(attrs={'class':'form-control'}),
            'description': forms.Textarea(attrs={'class':'form-control'}),
            'start_date': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
        }