from todolist.views import addToDo
from django.urls import path

from . import views

app_name = 'todolist'
urlpatterns = [
    # ex: /todolist/
    path('', views.index, name='index'),
    # ex: /todolist/add/
    path('add/', views.addToDo, name='addtodo'),
    # ex: /todolist/1/change
    path('<int:todo_id>/change/', views.changeParticipate, name='changetodo'),
    # ex: /todolist/1/update
    path('<int:todo_id>/update/', views.updateToDo, name='updatetodo'),
    # ex: /todolist/1/delete
    path('<int:todo_id>/delete/', views.deleteToDo, name='deletetodo'),
]