from datetime import date
from django.db.models.query import QuerySet
from django.http import HttpResponseRedirect
from django.http import Http404
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib import messages
from .models import ToDo
from .forms import ToDoForm

# Create your views here.
def index(request):
    """
    Return Homepage
    """
    template_name = 'todolist/index.html'
    todolist = ToDo.objects.filter().order_by('start_date')
    todo_dict = {}
    for item in todolist:
        date_key = timezone.localtime(item.start_date).date()
        todo_dict.setdefault(date_key, []).append(item)
    form = ToDoForm()
    context = {
        'todolist': todo_dict,
        'form': form,
        }
    return render(request, template_name, context)


def addToDo(request):
    if request.method == 'POST':
        form = ToDoForm(request.POST)
        form.full_clean()
        event_name = form.data['event_name']
        description = form.data['description']
        start_date = form.data['start_date']
        
        todo = ToDo(event_name=event_name, description=description, start_date=start_date)
        todo.save()
        messages.success(request, 'To-Do form successfully added!')
        return HttpResponseRedirect(reverse('todolist:index'))
        
        
    else:
        messages.warning(request, 'Error adding To-Do event')
        return HttpResponseRedirect(reverse('todolist:index'))

def changeParticipate(request, todo_id):
    if request.method == 'POST':
        todo = get_object_or_404(ToDo, pk=todo_id)
        todo.participate = not todo.participate
        todo.save()
        messages.success(request, 'Participate changed to ' + str(todo.participate))
        return HttpResponseRedirect(reverse('todolist:index'))
    else:
        messages.warning(request, 'Error changing the participate')
        return HttpResponseRedirect(reverse('todolist:index'))

def updateToDo(request, todo_id):
    old_todo = get_object_or_404(ToDo, pk=todo_id)
    old_todo.start_date = timezone.localtime(old_todo.start_date).strftime("%Y-%m-%dT%H:%M:%S")
    template_name = 'todolist/update.html'
    if request.method == 'GET':
        form = ToDoForm(instance=old_todo)
        context = {
            'name': old_todo.event_name,
            'id': old_todo.pk,
            'form': form,
            }
        return render(request, template_name, context)
    else:
        form = ToDoForm(request.POST)
        form.full_clean()
        old_todo.event_name = form.data['event_name']
        old_todo.description = form.data['description']
        old_todo.start_date = form.data['start_date']
        old_todo.save()
        messages.success(request, 'To-Do form successfully saved!')
        return HttpResponseRedirect(request.path_info)

def deleteToDo(request, todo_id):
    if request.method == 'POST':
        todo = get_object_or_404(ToDo, pk=todo_id)
        todo.delete()
        messages.success(request, 'successfully deleted!')
        return HttpResponseRedirect(reverse('todolist:index'))
    else:
        messages.warning(request, 'Error deleting To-Do event')
        return HttpResponseRedirect(reverse('todolist:index'))