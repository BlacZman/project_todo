FROM python:3-alpine
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE 1
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -r requirements.txt
COPY . /app/
RUN python manage.py migrate todolist
CMD [ "gunicorn", "projecttodo.wsgi:application", "--bind", "0.0.0.0:8000", "--workers=4" ]