# You can use "docker-compose up" on project folder to run this app service.

# You can use "docker build" to build an image to run this app.
// If you have python 3 you can run "pip install -r requirements.txt \ python manage.py runserver"

// Then you can access this app by open the browser and go to 'localhost:8000'

// !!! Not recommended because it can add unnescessary library to your python environment !!!